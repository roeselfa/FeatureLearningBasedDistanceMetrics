from pm4py.objects.log.adapters.pandas import csv_import_adapter
import sys

caseIDKey = "Case ID"
activityKey = "Activity"
durationKey = "Complete Timestamp"

originalLogPath = sys.argv[1]
sanitizedLogPath = sys.argv[2]

prePRETSAlogFrame = csv_import_adapter.import_dataframe_from_path(originalLogPath, sep=";")
prePRETSAtotalDuration = 0
for i in range(len(prePRETSAlogFrame.Duration)):
    prePRETSAtotalDuration += prePRETSAlogFrame.Duration[i]



logFrame = csv_import_adapter.import_dataframe_from_path(sanitizedLogPath, sep=";")
totalDuration = 0
traceLength = len(logFrame.Duration)
for i in range(traceLength):
    totalDuration += logFrame.Duration[i]

errorPerc = (abs(totalDuration - prePRETSAtotalDuration) / prePRETSAtotalDuration) * 100
print("Log:", sanitizedLogPath, ", Error: ", errorPerc, "%")

