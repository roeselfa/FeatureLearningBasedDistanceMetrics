from pm4py.objects.log.adapters.pandas import csv_import_adapter
from pm4py.objects.conversion.log import factory as conversion_factory
from pm4py.util import constants
import sys


def initializeChangeDic():
    changeDic = {"A to S": 0,
                 "A to N": 0,
                 "S to A": 0,
                 "S to N": 0,
                 "N to S": 0,
                 "N to A": 0}
    return changeDic


def getListFromDing(ding):
    l = list()
    for d in ding:
        l.append(d)

    return l


def getTracesFromLog(log):
    resultLog = list()
    for trace in log:
        exportLog = list()
        for event in trace:
            exportLog.append(event[activityKey])
        resultLog.append(exportLog)

    return resultLog


def getEventList(traceSet):
    events = list()
    for t in traceSet:
        for e in t:
            if e not in events:
                events.append(e)

    return events


def getPredecessorsOfEventInTrace(event, trace):
    predecessors = list()
    if event not in trace:
        return predecessors

    eventIndex = max(loc for loc, val in enumerate(trace) if val == event)
    restTrace = trace[:eventIndex]

    for e in restTrace:
        if e not in predecessors:
            predecessors.append(e)

    return predecessors


def getFollowersOfEventInTrace(event, trace):
    followers = list()
    if event not in trace:
        return followers
    eventIndex = trace.index(event)
    restTrace = trace[eventIndex + 1:]

    for e in restTrace:
        if e not in followers:
            followers.append(e)

    return followers


def getFollowsRelations(allEvents, traces):
    followsMatrix = {}

    for event in allEvents:
        alwaysFollows = allEvents.copy()
        neverFollows = allEvents.copy()

        for eClmn in allEvents:
            followsMatrix[(event, eClmn)] = 'S'

        for trace in traces:
            if event in trace:
                followers = getFollowersOfEventInTrace(event, trace)
                for f in followers:
                    if f in neverFollows:
                        neverFollows.remove(f)

                for e in allEvents:
                    if e not in followers:
                        if e in alwaysFollows:
                            alwaysFollows.remove(e)

        for a in alwaysFollows:
            followsMatrix[(event, a)] = 'A'

        for n in neverFollows:
            followsMatrix[(event, n)] = 'N'

    return followsMatrix


def getPrecedesRelations(allEvents, traces):
    precedesMatrix = {}

    for event in allEvents:
        alwaysPrecedes = allEvents.copy()
        neverPrecedes = allEvents.copy()

        for eClmn in allEvents:
            precedesMatrix[(event, eClmn)] = 'S'

        for trace in traces:
            if event in trace:
                predecessors = getPredecessorsOfEventInTrace(event, trace)
                for p in predecessors:
                    if p in neverPrecedes:
                        neverPrecedes.remove(p)

                for e in allEvents:
                    if e not in predecessors:
                        if e in alwaysPrecedes:
                            alwaysPrecedes.remove(e)

        for a in alwaysPrecedes:
            precedesMatrix[(event, a)] = 'A'

        for n in neverPrecedes:
            precedesMatrix[(event, n)] = 'N'

    return precedesMatrix


def getIntersectionSize(set1, set2, events):
    counter = 0
    for e1 in events:
        for e2 in events:
            if set1[(e1, e2)] == set2[(e1, e2)]:
                counter += 1
    return counter


def getBehavioralAppropriatenessScore(prePretsaFollows, prePretsaPrecedes, postPretsaFollows, postPretsaPrecedes,
                                      events):
    followsIntersectSize = getIntersectionSize(prePretsaFollows, postPretsaFollows, events)
    precedesIntersectSize = getIntersectionSize(prePretsaPrecedes, postPretsaPrecedes, events)
    followsSize = len(postPretsaFollows)
    precedesSize = len(postPretsaPrecedes)
    score = (followsIntersectSize / (2 * followsSize)) + (precedesIntersectSize / (2 * precedesSize))
    return score


caseIDKey = "Case ID"
activityKey = "Activity"
durationKey = "Complete Timestamp"


def main():
    originalLogPath = sys.argv[1]
    sanitizedLogPath = sys.argv[2]

    prePretsaFrame = csv_import_adapter.import_dataframe_from_path(originalLogPath, sep=";")

    prePetsaLog = conversion_factory.apply(prePretsaFrame,
                                           parameters={constants.PARAMETER_CONSTANT_CASEID_KEY: caseIDKey,
                                                       constants.PARAMETER_CONSTANT_ACTIVITY_KEY: activityKey,
                                                       constants.PARAMETER_CONSTANT_TIMESTAMP_KEY: durationKey})

    prePretsaTraces = getTracesFromLog(prePetsaLog)
    uniqueEventList = getEventList(prePretsaTraces)

    prePretsaFollowsRelations = getFollowsRelations(uniqueEventList, prePretsaTraces)
    prePretsaPrecedesRelations = getPrecedesRelations(uniqueEventList, prePretsaTraces)

    postPretsaFrame = csv_import_adapter.import_dataframe_from_path(sanitizedLogPath, sep=";")

    postPetsaLog = conversion_factory.apply(postPretsaFrame,
                                            parameters={constants.PARAMETER_CONSTANT_CASEID_KEY: caseIDKey,
                                                        constants.PARAMETER_CONSTANT_ACTIVITY_KEY: activityKey,
                                                        constants.PARAMETER_CONSTANT_TIMESTAMP_KEY: durationKey})
    postPretsaTraces = getTracesFromLog(postPetsaLog)

    postPretsaFollowsRelations = getFollowsRelations(uniqueEventList, postPretsaTraces)
    postPretsaPrecedesRelations = getPrecedesRelations(uniqueEventList, postPretsaTraces)

    baScore = getBehavioralAppropriatenessScore(prePretsaFollowsRelations,
                                                prePretsaPrecedesRelations,
                                                postPretsaFollowsRelations,
                                                prePretsaPrecedesRelations,
                                                uniqueEventList)

    print("Log:", sanitizedLogPath, ", Score: ", baScore)
